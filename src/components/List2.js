import React from 'react';
import PropTypes from 'prop-types';
import { render } from '../../setupTests';

class List extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: 'myname'
    }
  }

  onChangeHandler(event) {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value })
  };

  submitHandler(event) {
    event.preventDefault();
    return this.state;
  }

  render() {
    const { items } = this.props;
    if (!items.length) {
      return <span className="empty-message">No items in list</span>
    }
    return (
      <div>
        <ul className="list-items">
          {items.map(item => <li key={item} className="item">{item}</li>)}
        </ul>
        <div>
          <input type="text" name="username" onChange={this.onChangeHandler.bind(this)} />
          <button onClick={this.submitHandler.bind(this)}></button>
        </div>
      </div>
    );
  }
}

List.propTypes = {
  items: PropTypes.array,
}

List.defaultProps = {
  items: [],
}

export default List;
