import React from 'react';
import { mount, render } from '../../../setupTests';

import List from '../List';

describe('List Tests', () => {
  describe('Full Dom Rendering', () => {
    it('renders list-items', () => {
      const items = ['one', 'two', 'three'];
      const wrapper = mount(<List items={items} />);
      // console.log(wrapper.debug());
  
      expect(wrapper.find('.list-items')).toBeDefined();
      expect(wrapper.find('.item')).toHaveLength(items.length);
    });
  
    it('renders a list item', () => {
      const items = ['Thor', 'Loki'];
      const wrapper = mount(<List items={items} />);
      expect(wrapper.contains(<li key="Thor" className="item">Thor</li>)).toBeTruthy();
    });
  
    it('renders correcttext in items', () => {
      const items = ['John', 'James', 'Luke'];
      const wrapper = mount(<List items={items} />);
  
      expect(wrapper.find('.item').get(0).props.children).toEqual('John')
    });
  });

  describe('Static Rendering', () => {
    // static rendering is base on cheerio library
    // https://github.com/cheeriojs/cheerio

    it('renders list-items', () => {
      const items = ['one', 'two', 'three'];
      const wrapper = render(<List items={items} />);
      wrapper.addClass('foo');

      expect(wrapper.find('.list-items')).toBeDefined();
      expect(wrapper.find('.item')).toHaveLength(items.length);
    });

    it('renders a list item', () => {
      const items = ['Thor', 'Loki'];
      const wrapper = render(<List items={items} />);
      expect(wrapper.find(<li key="Thor" className="item">Thor</li>)).toBeTruthy();
    });
  
    it('renders correcttext in items', () => {
      const items = ['John', 'James', 'Luke'];
      const wrapper = render(<List items={items} />);

      // get the content of the render
      expect(wrapper.contents().length).toBe(3);

      // console.log(wrapper.children(2).tagName)
      // console.log(wrapper.children().get(2).parentNode.name)

      // get the text content of the selected element
      expect(wrapper.children().get(2).children[0].data).toBe('Luke');

      // get the parent of the render selected element
      expect(wrapper.children().get(1).parentNode.name).toBe('ul');

      // check the tagName of the a selected child
      expect(wrapper.children().get(2).tagName).toBe('li');

      // no index specified, returned an array of matches
      expect(wrapper.children().get().length).toBe(3);
      // expect(wrapper.children().get(1).attr)
      // expect(wrapper.children().get(1)).toEqual('James')
      expect(wrapper.children().first().text()).toEqual('John');
      expect(wrapper.children().last().text()).toEqual('Luke')

    });
  })
})