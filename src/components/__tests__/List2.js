import React from 'react';
import { shallow } from '../../../setupTests';

import List from '../List2';

describe('List Tests', () => {
  describe('Suite 1', () => {
    const items = ['one', 'two', 'three'];
    let wrapper;

    beforeAll(() => {
      wrapper = shallow(<List items={items} />);
    });

    it('renders list-items', () => {
      // console.log(wrapper.debug());

      console.log(wrapper.find('ul').type())

      // find an element and specify the prop you like to check for
      // note how different this is from props() which does not require argument
      // and returns the entire props of the element
      expect(wrapper.find('ul').prop('className')).toEqual('list-items');

      expect(wrapper.find('.list-items')).toBeDefined();
      expect(wrapper.find('.item')).toHaveLength(items.length);
      expect(wrapper.find('ul').first().children().length).toBe(3);

      // use childAt to get child at the specified index
      expect(wrapper.find('ul').childAt(1).type()).toBe('li');
      expect(wrapper.find('ul').childAt(1).props().className).toBe('item')
      expect(wrapper.find('ul').childAt(1).props().children).toBe('two')
      expect(wrapper.find('ul').childAt(1).children().props()).toBeDefined();
      expect(wrapper.find('ul').childAt(1).children().text()).toBe('two')
      expect(wrapper.find('ul').childAt(1).children().parent().length).toBe(1)
    });
  })

  describe('Suite 2', () => {
    const items = ['Thor', 'Loki'];
    let wrapper;

    beforeAll(() => {
      wrapper = shallow(<List items={items} />);
    });


    it('get element using at()', () => {
      // console.log(wrapper.find('li').at(1).text());
      // using the at method to get the element at specific index [0 based]
      expect(wrapper.find('li').at(1).children().props()).toBeDefined()
      expect(wrapper.find('li').at(1).text()).toBe('Loki')
      expect(wrapper.find('li').at(1).children().length).toBe(1)
      expect(wrapper.find('li').at(1).children().text()).toBe('Loki')
      expect(wrapper.find('li').at(1).props().children).toBe('Loki')
      expect(wrapper.find('li').at(1).props().className).toBe('item')
      expect(wrapper.contains(<li key="Thor" className="item">Thor</li>)).toBeTruthy();
    });

    it('get element using the get()', () => {
      // get() method return the props property to get the props of the node
      // console.log(wrapper.find('li').get(0))
      // the value of the props for the return node
      expect(wrapper.find('li').get(0).props.className).toBe('item')
      expect(wrapper.find('li').get(0).props.children).toBe('Thor')
    });

    it('use the first() child selection method', () => {
      // the method also work the same way with last()

      // console.log(wrapper.find('li').first().closest('div').props().children[0]);

      // returned methods -> text(), children(), props(), parent(), parent()
      // length represent the number of direct children node
      // when it contain only text 
      // children() return wrapper of the children node
      expect(wrapper.find('li').first().children().text()).toBe('Thor');
      expect(wrapper.find('li').first().type()).toBe('li');
      expect(wrapper.find('li').first().children().length).toBe(1);

      // text() return the text content of the node
      expect(wrapper.find('li').first().text()).toBe('Thor');

      // use props() to get the props of the node
      expect(wrapper.find('li').first().props().className).toBe('item');
      expect(wrapper.find('li').first().props().children).toBe('Thor');

      // use parent() to get the parent node
      expect(wrapper.find('li').first().parent().name()).toBe('ul');
      expect(wrapper.find('li').first().parent().length).toBe(1)
      expect(wrapper.find('li').first().parents().length).toBe(2);

      // use closet() to get the nearest type of element
      // .props(), .children() -> wrapper, .text() -> will get children text
      expect(wrapper.find('li').first().closest('div').length).toBe(1);
      expect(wrapper.find('li').first().closest('div').children().first().name()).toBe('ul')
      expect(wrapper.find('li').first().closest('div').children().first().is('.list-items')).toBeTruthy()
      expect(wrapper.find('li').first().closest('div').props().children[0].props.className).toBe('list-items')
    })
  });

  describe('Suite 3', () => {
    const items = ['John', 'James', 'Luke'];  
    let wrapper
    beforeAll(() => {
      wrapper = shallow(<List items={items} />);
    })
    it('renders correcttext in items', () => {

      // console.log(wrapper.props());
      // console.log(wrapper.setProps)

      // confirm element attribute using the is(selector)
      expect(wrapper.is('div')).toBeTruthy()
      expect(wrapper.childAt(0).is('.list-items')).toBeTruthy();
      expect(wrapper.childAt(0).is('[className="list-items"]')).toBeTruthy();

      // use hasClass to confirm class name
      expect(wrapper.hasClass('myclass')).toBeFalsy();
      expect(wrapper.childAt(0).hasClass('list-items')).toBeTruthy()
      expect(wrapper.childAt(0).childAt(1).hasClass('item')).toBeTruthy()
      expect(wrapper.find('.item').get(0).props.children).toEqual('John')

      // not() return elements that are not match by the selector
      expect(wrapper.not('.myclass')).toBeTruthy();
      // expect(wrapper.childAt(0).not('.list-items')).toBeFalsy();
      expect(wrapper.childAt(0).not("ist-items")).toHaveLength(1);
      expect(wrapper.not("ist-items")).toHaveLength(1);
      expect(wrapper.childAt(0).children().not(".items").length).toBe(3)

      // use name() to get the name of the tag
      expect(wrapper.name()).toBe('div');
      expect(wrapper.childAt(0).name()).toBe('ul');
      expect(wrapper.childAt(0).children().last().name()).toBe('li')
      expect(wrapper.childAt(0).children().first().name()).toBe('li')
      expect(wrapper.childAt(0).children().at(1).name()).toBe('li')

      // use state to get the state of the componet
      expect(wrapper.state().name).toBe('myname');
      expect(wrapper.state('name')).toBe('myname');

      // use setState to set the state of the component
      // set username on the state of the component
      wrapper.setState({ username: 'username'});

      // confirm the state is updated
      expect(wrapper.state('name')).toBe('myname');
      expect(wrapper.state('username')).toBe('username');

      // use setProps to set property of the component

    });

    it('simulate events', () => {
      // console.log(wrapper.find('button').simulate('click', {
      //   preventDefault: () => {},
      //   target: {
      //     name: 'myname',
      //     value: 'godfrey',
      //   }
      // }))

      // console.log(wrapper.state())
      // you can get access to component methods through the instance()
      // and call the method passing params and events
      const res = wrapper.instance().submitHandler({
        preventDefault: () => {},
        target: {
          name: 'username',
          value: 'Warame'
        }
      })

      // simulate events, if the event require certain
      // props passing such props as demonstrated below
      wrapper.find('input').simulate('change', {
        preventDefault: () => {},
        target: {
          name: 'username',
          value: 'Esanye'
        }
      });
      // console.log(res, 'HHHHHH')

    });
  });

  describe('mocking fumctions and methods', () => {
    const items = ['John', 'James', 'Luke'];  
    let wrapper
    beforeAll(() => {
      wrapper = shallow(<List items={items} />);
    });

    it('should call the change handler', () => {
      const submitHandler = jest.fn();
      wrapper.instance().submitHandler = submitHandler;
      wrapper.instance().submitHandler('click', {
        preventDefault: () => {}
      });
      expect(wrapper.instance().submitHandler).toHaveBeenCalled()
    });


    it('should call the change handler', () => {
      jest.spyOn(wrapper.instance(), 'submitHandler')
      wrapper.find('button').simulate('click', {
        preventDefault: () => {}
      });
      expect(wrapper.instance().submitHandler).toHaveBeenCalled();
      // console.log(wrapper.find('button').props())
    });

    it('call method from props', () => {
      const mockFn = jest.fn();
      List.prototype.onChangeHandler = mockFn
      let wrapper = shallow(<List items={items} />);
      wrapper.find('input').props().onChange();
      expect(mockFn).toHaveBeenCalledTimes(1)
    })
  });
});
